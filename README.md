# Information

Description: This is the REST version for the ATWD project.

function: able to read, create, update and delete data of a book.

scenario: It is a library book record management system which users can insert a book record, upload photos for a book, edit book data, delete a book and get the data of a book.

database type: no-sql (xml)

UI Library: Bootstrap


# To Do
* [x] build update function for book
* [x] build insert function for book
* [x] build function enable to search books
* [x] build function enable to delete a book record.
* [x] build ImageService class for uploading photos for books