<?php
interface RESTtemplate{
	public function restGet($urlSegment);
	public function restPost($urlSegment);
	public function restPut($urlSegment);
	public function restDelete($urlSegment);
}
?>