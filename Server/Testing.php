<?php
	$doc=new DOMDocument();
	
	$doc->load("books.xml");
	
	$books=$doc->getElementsByTagName("book");
	
	$output=array();
	
	foreach($books as $book){
		$output_item=array(
			"isbn"=>$book->getAttribute("isbn"),
			"title"=>$book->getElementsByTagName("title")->item(0)->nodeValue,
			"author"=>$book->getElementsByTagName("author")->item(0)->nodeValue,
			"genre"=>$book->getElementsByTagName("genre")->item(0)->nodeValue,
			"description"=>$book->getElementsByTagName("description")->item(0)->nodeValue,
			"imgPath"=>$book->getElementsByTagName("imgPath")->item(0)->nodeValue
		);
		
		$output[]=$output_item;
	}
	
	echo json_encode($output);
	
	echo "<br>";
	
	//Add new book
	/*
	$new=$doc->createElement("book");
	$doc->documentElement->appendChild($new);
	
	$new->setAttribute("isbn",2637);
	
	$title=$doc->createElement("title","Ubuntu: The Beginner's Guide");
	$new->appendChild($title);
	
	$author=$doc->createElement("author","Jonathan Moeller");
	$new->appendChild($author);
	
	$category=$doc->createElement("genre","Technical");
	$new->appendChild($category);
	
	$desc=$doc->createElement("description","It is a guide to users new to Ubuntu Linux to have a basic understanding on operating system,command line and advanced server configuration,etc.");
	$new->appendChild($desc);
	
	$rate=$doc->createElement("imgPath","../Server/images/Ubuntu_Guide.jpg");
	$new->appendChild($rate);
	
	$doc->save("books.xml");
	*/
	
	//delete a book
	/*
	$target=$doc->getElementById("3546");
	$doc->documentElement->removeChild($target);
	$doc->save("books.xml");
	*/
	
	//Testing to convert a string array into an associative array
	
	$ary=array("title=little prince","author=Saint-Exurpery");
	$out=array();
	
	foreach($ary as $item){
		$temp=explode("=",$item);
		$out[$temp[0]]=$temp[1];
	}
	foreach($out as $k=>$v){
		echo $k." ".$v."<br>";
	}
	
	//update
	$old_node=$doc->getElementById("1234");
	$ary=array(
		"title"=>"Animal Farm",
		"genre"=>"fable",
		"description"=>"it is a fable story where the animal thrown out the regime of the human of a farm."
	);
	
	foreach($ary as $key=>$value){
		$old_node->getElementsByTagName($key)->item(0)->nodeValue=$value;
	}
	
	$doc->save("books.xml");
?>