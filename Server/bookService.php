<?php
    require_once("RESTtemplate.php");

    class bookService implements RESTtemplate{
		
        //fetch data
        public function restGet($urlSegment){
			$xml=new DOMDocument();
			$xml->load("books.xml");
			
			$output=array();
			
			$books=$xml->getElementsByTagName("book");
			
			if(count($urlSegment)==0 || $urlSegment[0]==""){
				
				foreach($books as $book){
					$output_item=array(
						"isbn"=>$book->getAttribute("isbn"),
						"title"=>$book->getElementsByTagName("title")->item(0)->nodeValue,
						"author"=>$book->getElementsByTagName("author")->item(0)->nodeValue,
						"genre"=>$book->getElementsByTagName("genre")->item(0)->nodeValue,
						"description"=>$book->getElementsByTagName("description")->item(0)->nodeValue,
						"img"=>$book->getElementsByTagName("img")->item(0)->nodeValue
					);
				
					$output[]=$output_item;
				}
			}else{
				/*
					get elements inside the urlSegment and put them into associative array
					"ISBN=isbn"->"ISBN"=>isbn
					the result will be depends on the numbers of parameters
				*/
				$ary=array();
				
				/*
					ISBN=>isbn
					title=>title
					author=>author
					genre=>genre
				*/
				
				foreach($urlSegment as $seg){
					$temp=explode("=",$seg);
					$ary[$temp[0]]=$temp[1];
				}
			    /*
			    traverse each book_node, compare specific content with the associative array using something like preg_match(
				if match, push the content into the $output
				*/
				foreach($books as $book){
			        //initialize values in the $book
			        $isbn=$book->getAttribute("isbn");
			        $title=$book->getElementsByTagName("title")->item(0)->nodeValue;
			        $author=$book->getElementsByTagName("author")->item(0)->nodeValue;
			        $genre=$book->getElementsByTagName("genre")->item(0)->nodeValue;
			        $desc=$book->getElementsByTagName("description")->item(0)->nodeValue;
			        $img=$book->getElementsByTagName("img")->item(0)->nodeValue;
			       
					//check if they and the values within associative array are similar/same, put it in the $output[]
					$ary_k_exists_1=array_key_exists('ISBN',$ary);
					$ary_k_exists_2=array_key_exists('title',$ary);
					$ary_k_exists_3=array_key_exists('author',$ary);
				   
					if($ary_k_exists_1){
						if(strcmp($ary['ISBN'],$isbn)===0){
							$output_item=array(
								"isbn"=>$isbn,
								"title"=>$title,
								"author"=>$author,
								"genre"=>$genre,
								"description"=>$desc,
								"img"=>$img
							);
							$output[]=$output_item;
						}
					}
					
					if($ary_k_exists_2){
						$reg='/'.$ary['title'].'/';
						if(preg_match($reg,$title)===1){
							$output_item=array(
								"isbn"=>$isbn,
								"title"=>$title,
								"author"=>$author,
								"genre"=>$genre,
								"description"=>$desc,
								"img"=>$img
							);
						
							$output[]=$output_item;
						}
					}
					
					if($ary_k_exists_3){
						$reg='/'.$ary['author'].'/';
						if(preg_match($reg,$author)===1){
							$output_item=array(
								"isbn"=>$isbn,
								"title"=>$title,
								"author"=>$author,
								"genre"=>$genre,
								"description"=>$desc,
								"img"=>$img
							);
							
							$output[]=$output_item;
						}
					}
				   
					if(strcmp($ary['genre'],$genre)===0){
						$output_item=array(
							"isbn"=>$isbn,
							"title"=>$title,
							"author"=>$author,
							"genre"=>$genre,
							"description"=>$desc,
							"img"=>$img
						);
						
						$output[]=$output_item;
					}
				}
			}
			
			echo json_encode($output);
        }

        public function restPut($urlSegment){
            $xml=new DOMDocument();
			$xml->load("books.xml");
			
			$isbn=array_shift($urlSegment);
			$old_node=$xml->getElementById($isbn);
			
            $ary=array();
            
            foreach($urlSegment as $seg){
                $temp=explode("=",$seg);
                $ary[$temp[0]]=$temp[1];
            }
        
            //replace nodes within the same isbn
			foreach($ary as $key=>$value){
				$old_sub_node=$old_node->getElementsByTagName($key)->item(0)->nodeValue=$value;
			}
			
			$xml->save("books.xml");
			
			$output=array();
			$books=$xml->getElementsByTagName("book");
			
			foreach($books as $book){
				$output_item=array(
					"isbn"=>$book->getAttribute("isbn"),
					"title"=>$book->getElementsByTagName("title")->item(0)->nodeValue,
					"author"=>$book->getElementsByTagName("author")->item(0)->nodeValue,
					"genre"=>$book->getElementsByTagName("genre")->item(0)->nodeValue,
					"description"=>$book->getElementsByTagName("description")->item(0)->nodeValue,
					"img"=>$book->getElementsByTagName("img")->item(0)->nodeValue
				);
			
				$output[]=$output_item;
			}
			
			echo json_encode($output);
        }

        //delete data
        public function restDelete($urlSegment){
            //removing specific data by manipulating the xml file
			
			$xml=new DOMDocument();
			$xml->load("books.xml");
			
			//as the DELETE HTTP request only send out the isbn,just get the first index of urlSegment
			$isbn=$urlSegment[0];
			
			//echo gettype($isbn)." ".$isbn;
			
			$del_book=$xml->getElementById($isbn);
			
			//if the specific book has image store within the server, find and delete it.
			$imgPath=$del_book->getElementsByTagName("imgPath")->item(0)->nodeValue;
			if($imgPath!==""){
				$imgPath=explode("/",$imgPath);
				$imgPath="./".$imgPath[count($imgPath)-2]."/".$imgPath[count($imgPath)-1];
				unlink($imgPath);
			}
			
			$xml->documentElement->removeChild($del_book);
			
			$xml->save("books.xml");
			
			$books=$xml->getElementsByTagName("book");
			
			$output=array();
			
			foreach($books as $book){
				$output_item=array(
					"isbn"=>$book->getAttribute("isbn"),
					"title"=>$book->getElementsByTagName("title")->item(0)->nodeValue,
					"author"=>$book->getElementsByTagName("author")->item(0)->nodeValue,
					"genre"=>$book->getElementsByTagName("genre")->item(0)->nodeValue,
					"description"=>$book->getElementsByTagName("description")->item(0)->nodeValue,
					"img"=>$book->getElementsByTagName("img")->item(0)->nodeValue
				);
			
				$output[]=$output_item;
			}
			
			echo json_encode($output);
			
        }

        //create data
        public function restPost($urlSegment){
            //create data for what is included within the $urlSegment
			$xml=new DOMDocument();
			$xml->load("books.xml");
			
			$output=array();
			
			//it may first check if there is an redundant isbn
			if($xml->getElementById($urlSegment[0])==null){
			
				$new=$xml->createElement("book");
				$xml->documentElement->appendChild($new);
				$new->setAttribute("isbn",$urlSegment[0]);
			
				$title=$xml->createElement("title",$urlSegment[1]);
				$new->appendChild($title);
			
				$author=$xml->createElement("author",$urlSegment[2]);
				$new->appendChild($author);
			
				$genre=$xml->createElement("genre",$urlSegment[3]);
				$new->appendChild($genre);
			
				$desc=$xml->createElement("description",$urlSegment[4]);
				$new->appendChild($desc);
			
				//this is just a dummy
				$img=$xml->createElement("img","false");
				$new->appendChild($img);
			
				$xml->save("books.xml");
			}else{
				$message="The ISBN input has been used! Please try another number!";
				$output[]=$message;
			}
			
			$books=$xml->getElementsByTagName("book");
			
			foreach($books as $book){
				$output_item=array(
					"isbn"=>$book->getAttribute("isbn"),
					"title"=>$book->getElementsByTagName("title")->item(0)->nodeValue,
					"author"=>$book->getElementsByTagName("author")->item(0)->nodeValue,
					"genre"=>$book->getElementsByTagName("genre")->item(0)->nodeValue,
					"description"=>$book->getElementsByTagName("description")->item(0)->nodeValue,
					"img"=>$book->getElementsByTagName("img")->item(0)->nodeValue
				);
				$output[]=$output_item;
			}
			echo json_encode($output);
        }
    }
?>