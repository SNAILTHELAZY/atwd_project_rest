<?php
	require_once("RESTtemplate.php");
	
	class imageService{
		private $filename;
		private $location;
		private $tmp_location;
		
		public function __construct($formdata){
			$this->filename=$formdata['file']['name'];
			$this->location="Images/".$this->filename;
			$this->tmp_location=$formdata['file']['tmp_name'];
		}
		public function restGet($urlSegment){}
		public function restPost($urlSegment){
			$isbn=$urlSegment[0];
			
			if(move_uploaded_file($this->tmp_location,$this->location)){
				//change data of specific isbn and rename the file
				$newname="Images/".$isbn.".jpg";
				rename($this->location,$newname);
				$xml=new DOMDocument();
				$xml->load("books.xml");
				
				$book=$xml->getElementById($isbn);
				$book->getElementsByTagName("img")->item(0)->nodeValue="true";
				
				$xml->save("books.xml");
				
				//get all book to the client side
				$books=$xml->getElementsByTagName('book');
				
				$output=array();
				foreach($books as $book){
					$output_item=array(
						"isbn"=>$book->getAttribute("isbn"),
						"title"=>$book->getElementsByTagName("title")->item(0)->nodeValue,
						"author"=>$book->getElementsByTagName("author")->item(0)->nodeValue,
						"genre"=>$book->getElementsByTagName("genre")->item(0)->nodeValue,
						"description"=>$book->getElementsByTagName("description")->item(0)->nodeValue,
						"img"=>$book->getElementsByTagName("img")->item(0)->nodeValue
					);
					$output[]=$output_item;
				}
				
				echo json_encode($output);
			}else{
				$msg="cannot fetch file from the temporary storage place.";
				echo json_encode($msg);
			}
		}
		public function restPut($urlSegment){}
		public function restDelete($urlSegment){}
	}
?>