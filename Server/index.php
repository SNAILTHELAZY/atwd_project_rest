<?php
class Controller{
	private $serviceProvider=false;
	private $urlSegment=false;
	
	public function __construct(){
		if(!isset($_SERVER['PATH_INFO']) || $_SERVER['PATH_INFO']=='/'){
			http_response_code(400);
			return;
		}
		
		$this->urlSegment=$_SERVER['PATH_INFO'];
		$this->urlSegment=explode('/',$this->urlSegment);
		array_shift($this->urlSegment);
		
		$resource=array_shift($this->urlSegment);
		$resource=ucfirst(strtolower($resource));
		
		$resource=$resource."Service";
		$resourceFile=$resource.".php";
		
		if(file_exists($resourceFile)){
			require_once $resourceFile;
			if(isset($_FILES)){
				$this->serviceProvider=new $resource($_FILES);
			}else
				$this->serviceProvider=new $resource;
		}else{
			http_response_code(404);
			return;
		}
	}
	
	public function run(){
		if($this->serviceProvider===false){
			http_response_code(400);
			return;
		}
		$method=$_SERVER['REQUEST_METHOD'];
		$method=ucfirst(strtolower($method));
		$func='rest'.$method;
		
		$this->serviceProvider->$func($this->urlSegment);
	}
}

$c=new Controller();
$c->run();
?>